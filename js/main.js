'use strict';

const nav = document.querySelector('.navigation');
const logo = document.querySelector('.headerLogo');
const navOffsetTop = nav.offsetTop;


window.addEventListener('scroll', function() {
    if (window.scrollY > navOffsetTop)  {
        nav.style.position = 'fixed';
        nav.style.top = 0;
        nav.style.width = '100%';
        logo.style.width = '2rem';
        logo.style.transition = "width 1s ease-in-out 0s";
    }
    else{
        logo.style.width = '4rem';
        logo.style.transition = "width 1s ease-in-out 0s";
    }
});

document.querySelector("#firstname").addEventListener("input", function() {
    document.querySelector("#showFirstname").innerHTML = this.value;
});

document.querySelector("#lastname").addEventListener("input", function(){
    document.querySelector("#showLastname").innerHTML = this.value;
});

document.querySelector("#slogan").addEventListener("change", function() {    
    document.querySelector("#showSlogan").innerHTML = this.value;
});

document.querySelector("#color").addEventListener("change", function() {
    changeTextColor(this.value);
});

function changeTextColor(color) {
    const backColor = color + 'Back';
    const textColor = color + "Word";
    const borderColor = color + 'Border';
    document.querySelector("#showFirstname").className ='';
    document.querySelector("#showFirstname").classList.add(backColor);
    document.querySelector("#showLastname").className ='';
    document.querySelector("#showLastname").classList.add(backColor);
    document.querySelector("#marseille").className = '';
    document.querySelector("#marseille").classList.add(backColor);
    document.querySelector("#showSlogan").className = '';
    document.querySelector("#showSlogan").classList.add(textColor);
    document.querySelector("#titleColor").className = '';
    document.querySelector("#titleColor").classList.add(textColor);
    document.querySelector("#output_image").className = '';
    document.querySelector("#output_image").classList.add(borderColor);
    document.querySelector(".logoBleu").classList.toggle("hidden");
    document.querySelector(".logoRose").classList.toggle("hidden");
}

function preview_image(event) 
{
 const reader = new FileReader();
 reader.onload = function()
 {
  const output = document.getElementById('output_image');
  output.src = reader.result;
 }
 reader.readAsDataURL(event.target.files[0]);
}